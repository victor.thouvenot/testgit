from datetime import datetime, timedelta
import daylight, pytz
from astral import LocationInfo,sun


def get_solar_noon_greenwich(date_py):
    """
    Renvoie le midi solaire à Greenwich en heure UTC
    
    Args:
        date_py(string): La date au format datetime de python    
    
    Returns:
        string : le midi solaire à Greenwich en UTC  
    """
    city=LocationInfo("Greenwich")
    s = sun.sun(city.observer,date_py)
    return get_datetime_python(f'{s["noon"]}')
    

def get_solar_noon_at_coordinates(lat, long, date):
    """
    Revoie le midi solaire aux coordonnées et à la date données
    (Change de 1 seconde tous les 0.006 de longitude environ)
    
    Args:
        date(string): La date au format datetime de python
        
        lat(float): une latitude 
        
        long(float): une longitude

    Returns:
        string : le midi solaire aux coordonnées en heure UTC 
    """
    sun = daylight.Sunclock(lat, long)
    t = sun.solar_noon(date.timestamp())
    return datetime.utcfromtimestamp(t)


def datetime_python_to_datetime_json(datetime_python):
    """
    Retourne datetime_python sous format datetime_json
    
    Args:
        datetime_python(string): une date au format datetime de python

    Returns:
        float : la date normalisé (format datetime_json)
    """
    mon_timestamp=datetime.timestamp(datetime_python) #nombre de seconde depuis 1970,0,0
    annee_timestamp=datetime_python.year
    delta_timestamp=mon_timestamp-datetime.timestamp(datetime(annee_timestamp-1, 12, 31,0,0,0))
    return annee_timestamp+(delta_timestamp/(60*60*24*365))



def get_datetime_python(str_datetime):
    """
    Renvoie un objet datetime de python à partir d'une chaine de caractère  
    
    Args:
        str_datetime(string): le datetime d'une photo

    Returns:
        datetime_python :  la date et l'heure au format datetime de python
    """
    date_time_obj = datetime.fromisoformat(str_datetime)
    return date_time_obj


def datetime_python_to_time_of_day(datetime_py):
    '''
    Renvoie le time_of_day d'un datetime python
    
    Args:
        datetime_py(datetime_python) : un objet datetime de python
    
    Returns:
        time_of_day(float) : le time_of_day correspondant à ce datetime
    '''
    return duree_to_time_of_day(datetime_py.hour * 60 * 60 + datetime_py.minute * 60 + datetime_py.second)


def duree_to_time_of_day(duree):
    """
    Renvoie un time_of_day à partir d'une duree en seconde
    
    Args:
        duree(entier): une duree en seconde (ou un timestamp)
    
    Returns: 
        float : la duree sous format time of day
    """
    return duree/(86400) # == duree/(60*60*24)


def jour_numero(datetime_py):
    """
    Renvoie le numéro du jour de l'année d'une date
    
    Args:
        datetime_py (datetime_python): un objet datetime de python

    Returns:
        entier : numéro du jour de l'année 
    """
    y = datetime_py.year
    delta = datetime_py - datetime(year=y, month=1, day=1)
    return delta.days + 1


def afficher_time_of_day(tod):
    """
    Affiche le time_of_day sous format heure:minute:seconde

    Args:
        tod(float): un time_of_day
    Returns:
        void
    """
    h=int(tod*24)
    m=int((tod*24%1)*60)
    s=int(((tod*24%1)*60%1)*60)
    print(h,":",m,":",s)


def jours_to_datetime_py(jour,annee):
    """
    Renvoie un datetime python à partir d'un nombre de jour et d'une année

    Args:
        jour (entier): le numéro du jour 
        annee (entier)

    Returns:
        datetime_python : un objet datetime python à partir de l'année et du jour
    """
    jour = jour.rjust(3, '0') 
    annee = annee.rjust(4, '0')
    res = datetime.strptime(annee + "-" + jour, "%Y-%j").strftime("%Y-%m-%d") 
    return datetime.fromisoformat(res) 


def datetime_to_datime_py(datetime):
    """
    Renvoie un objet datetime _python à partir d'un datetime_json

    Args:
        datetime (float): datetime_json 

    Returns:
        datetime_python : un objet datetime python 
    """
    year=str(int(datetime))
    jours=str(int((datetime-int(year))*365)+1)
    return jours_to_datetime_py(jours,year)


def datetime_to_jours(datetime):
    """
    Renvoie une duree en jour

    Args:
        datetime (float): datetime_json

    Returns:
        float : duree en jour
    """
    year=int(datetime)
    jours=(datetime-int(year))*365
    return 365*year + jours


def dans_periode_instable(datetime_py):
    """
    Retourne vrai si jour est dans une des deux périodes instable (autour du 21 mars et du 21 sept.) ([0.13737811484290338, 0.297245008512614] et [0.6673038229376255, 0.7967265129236958])

    Args:
        datetime_py(datime_python) : un objet datetime de python
    
    Returns:
        booleen : vrai si dans la période instable, faux sinon
    """
    jour = datetime_python_to_datetime_json(datetime_py)%1
    return (jour >= 0.13737811484290338 and jour <= 0.297245008512614) or (jour >= 0.6673038229376255 and jour <= 0.7967265129236958)
