from class_photoset import photoset 


def photo_set_partiel_2point0(data, duree, nb_min_photos, duree_max):
    """Realise une partition du photo_set de base, en se basant sur la duree (en jours). Si moins de nb_min_photos
    dans une partition, alors la fonction va chercher les photos suivantes tant que le nombre min n'est pas atteint.
    Si le nombre min de photos n'est pas atteint en duree_max, alors la partition courante est annulée (on ne la garde pas)
    et on passe à la partition suivante (qui débutera à la premiere photo rajoutee au dela de duree pour la partition precedente)  
    Toutefois, la fonction repars de la premiere photo rajoutee pour la partition suivante (possibilité de chevauchement
    entre les partitions)

    Args:
        data (class photoset): un set de photo
        duree (int): duree en jours des sous partitions à créer
        nb_min_photos (int): nombre de photos minimal qui doit etre présent dans une partition
        duree_max (int): (en jour) duree maximale autorisée pour une partition elargie ne possédant pas assez de photos sur duree

    Returns:
        list: Liste des partitions correspondante
    """
    
    # Trier la liste de l'ensemble des photos
    # par datetime
    ps_sorted = sorted(data.usefuldata("photos"),
                       key=lambda d: d["datetime"])
    liste_photo_set_partiel = []
    #convertir la duree en datetime
    duree = duree/(365.0)
    duree_max = duree_max/(365.0)
    
    # initialisation des parametres
    photo_set_partiel = []
    nb_photo_set_courant = 0
    p = 0
    p_mem = 0
    
    while p < len(ps_sorted):
        datetime_ac = ps_sorted[p]["datetime"]
        # si p==p_mem, alors on est dans le cas ou l'on debute une nouvelle partition
        if p == p_mem:
            datetime_start = datetime_ac
        # Si la photo est hors de la duree demandee
        # on rentre dans un nouvelle partition 
        if (datetime_ac-datetime_start) > duree:
            # on memorise ma premiere photo suivante
            p_mem = p
            # si le nombre minimal de photos n'es pas atteint, on continue d'ajouter des photos jusqu'à atteindre la limite max, la fin du photo_set ou atteindre le nb de photo minimal
            if nb_photo_set_courant < nb_min_photos:
                while p < len(ps_sorted)-1 and (datetime_ac-datetime_start) < duree_max and nb_photo_set_courant < nb_min_photos:
                    photo_set_partiel.append(ps_sorted[p])
                    nb_photo_set_courant += 1
                    p += 1
                    datetime_ac = ps_sorted[p]["datetime"]
            # si le nombre de photo minimal est atteint, on ajoute le set, sinon on ne l'ajoute pas
            if nb_photo_set_courant >= nb_min_photos:
                liste_photo_set_partiel.append(photo_set_partiel)
            
            # réinitialisation des parametres pour la prochiane partition
            photo_set_partiel = []
            p = p_mem - 1 # p va etre incremente en fin de boucle, d'ou p_mem-1
            nb_photo_set_courant = 0
            
        # Si on est dans la meme partition de duree
        else:
            photo_set_partiel.append(ps_sorted[p])
            nb_photo_set_courant += 1

        p += 1
    
    # Retourne la liste de partitions
    liste_photo_set_partiel.append(photo_set_partiel)
    return liste_photo_set_partiel
    
# calcul la concentration des valeurs contenues dans values dans les intervals de taille 100/nb_inter
def concentration(values, nb_inter=10):
    """Calcul la concentration des valeurs de time_of_day (comprises dans [0;1]) selon 
    une partition de nb_inter intervals de [0;1]

    Args:
        values (list): liste de valeurs
        nb_inter (int, optional): le nombre d'intervals à creer dans [0;1]. Si 100 n'est pas divisible par nb_inter --> retourne erreur et liste vide. Defaults to 10.

    Returns:
        list: liste de liste, la ieme liste étant la liste des valeurs comprises dans l'interval [i/100, (i+100/nb_inter)/100]
    """
    # retourne message d'erreur si nb_inter ne divise pas 100
    if 100 % nb_inter != 0:
        print("Erreur : La taille des sous-intervals demandée n'est pas un multiple de 100 !")
        return []

    # création des intervals
    intervals = []
    for i in range(0, 100, 100//nb_inter):
        intervals += [[i/100, (i+100/nb_inter)/100]]

    densite = [0 for i in range(0,nb_inter)]
    densite_values = [[] for i in range(0,nb_inter)]
    for v in values:
        k = 0
        for i in intervals:
            if v >= i[0] and v < i[1]:
                densite_values[k].append(v)
            k += 1

    return densite_values

def photo_set_partiel(data, duree):
    """Crée des sous partitions de duree du photoset global
    La fonction n'est plus utilisée, remplacée par son évolution photo_set_partiel_2point0

    Args:
        data (class photoset): un set de photo
        duree (int): duree en jours des sous partitions à créer
    Returns:
        list: Liste des partitions correspondante
    """
    # Trier la liste de l'ensemble des photos
    # par datetime
    ps_sorted = sorted(data.usefuldata("photos"),
                       key=lambda d: d["datetime"])
    liste_photo_set_partiel = []
    #convertir la duree en datetime
    duree = duree/(365.0)
    photo_set_partiel = []
    datetime_start = ps_sorted[0]["datetime"]
    for photo in ps_sorted:
        datetime_ac = photo["datetime"]
        # Si la photo est hors de la duree demander 
        # On rentre dans une autre partition
        if (datetime_ac-datetime_start > duree):
            liste_photo_set_partiel.append(photo_set_partiel)
            photo_set_partiel = []
            photo_set_partiel.append(photo)
            datetime_start = datetime_ac
        # Si on est dans la meme partition de duree
        else:
            photo_set_partiel.append(photo)
    # Retourn la partition
    liste_photo_set_partiel.append(photo_set_partiel)
    return liste_photo_set_partiel


# prend en parametres une partition du set de photo (sous-set)
# def lever_coucher(photo_set):
def lever_coucher(data):
    """Renvoie le lever et le coucher du soleil 
    de l'ensemble des photo obtenu grace aux valeurs de daylight et time_of_day des photos du set data

    Args:
        data (class photoset): photoset à analyser

    Returns:
        tuple (tod_lever, tod_coucher): sous format time_of_day l'heure de lever et de coucher du soleil
    """
    # avec daylight
    time_of_day = [ sub['time_of_day'] for sub in data ]
    daylight = [ sub['daylight'] for sub in data ]
    # valeur prises en dessous de 0.1 de daylight pour calculer debut et fin du jour
    epsilon = 0.1
    time_low_daylight=[]
    time_high_daylight=[]
    there_is_daylight_low = False
    k = 0
    for d in daylight:
        if d < epsilon:
            time_low_daylight.append(time_of_day[k])
            there_is_daylight_low = True
        else:
            time_high_daylight.append(time_of_day[k])
        k += 1

    densite_high_daylight = concentration(time_high_daylight)

    if there_is_daylight_low:
        densite_low_daylight = concentration(time_low_daylight) 

        # on determine si le jour est à cheval sur 1-0 ou si il est dans l'interval
        if len(densite_high_daylight[0]) > 0 and len(densite_high_daylight[-1]) > 0:
            # on est ici dans le cas ou le jour est a cheval sur 1-0 (gps/d642bf05.json)
            # dans ce cas, le début du jour est dans l'inteval le max de time_low_daylight
            tod_lever = max(time_low_daylight)
            # et la fin du jour est le min de time_low_daylight
            tod_coucher = min(time_low_daylight)
        else:
            # on est ici dans le cas ou le jour est centre dans 0-1 (no_gps/0b34ff9e.json)
            # determination de l'heure de lever
            indice_max = 0
            # on avance tant qu'il n'y a pas de valeurs low
            while len(densite_low_daylight[indice_max]) < 1:
                indice_max += 1
            # quand il y en a, on avance jusqu'à ce qu'il n'y en ai plus
            while len(densite_low_daylight[indice_max]) > 0:
                indice_max += 1
            indice_max -= 1
            tod_lever = max(densite_low_daylight[indice_max])

            # determination de l'heure de coucher, meme chose mais en partant du dernier interval
            indice_max = -1
            # on avance tant qu'il n'y a pas de valeurs low
            while len(densite_low_daylight[indice_max]) < 1:
                indice_max -= 1
            # quand il y en a, on avance jusqu'à ce qu'il n'y en ai plus
            while len(densite_low_daylight[indice_max]) > 0:
                indice_max -= 1
            indice_max += 1
            tod_coucher = min(densite_low_daylight[indice_max])

    else:
        # ici, il n'y a pas de données avec une daylight basse dans le set de données.
        # on prend donc les valeurs par rapport aux valeurs de début et de fin des valeurs hautes
        # on determine si le jour est à cheval sur 1-0 ou si il est dans l'interval
        if len(densite_high_daylight[0]) > 0 and len(densite_high_daylight[-1]) > 0:
            # on est ici dans le cas ou le jour est a cheval sur 1-0 (gps/d642bf05.json)
            # determination de l'heure de coucher
            indice_max = 0
            # on avance tant qu'il y a des valeurs high
            while len(densite_high_daylight[indice_max]) > 0:
                indice_max += 1
            indice_max -= 1
            tod_coucher = max(densite_high_daylight[indice_max])

            # determination de l'heure de lever, meme chose mais en partant du dernier interval
            indice_max = -1
            # on avance tant qu'il y a des valeurs high
            while len(densite_high_daylight[indice_max]) > 0:
                indice_max -= 1
            indice_max += 1
            tod_lever = max(densite_high_daylight[indice_max])
        else:
            # on est ici dans le cas ou le jour est centre dans 0-1 (no_gps/0b34ff9e.json)
            # dans ce cas, le début du jour est dans l'inteval le min de time_high_daylight
            tod_lever = min(time_high_daylight)
            # et la fin du jour est le max de time_high_daylight
            tod_coucher = max(time_high_daylight)

    return tod_lever, tod_coucher

def calculer_duree_du_jour(tod_lever, tod_coucher, avec_minimisation=True):
    """Calcul la duree du jour grace aux valeurs de lever et de coucher du soleil, applique les résultats de la minimisation ou non (par defaut oui)

    Args:
        tod_lever (float entre 0 et 1): heure de lever en time_of_day
        tod_coucher (float entre 0 et 1): heure de coucher en time_of_day
        avec_minimisation (bool, optional): Si True, alors la minimisation trouvee de la duree du jour par rapport à la daylight est activée. Defaults to True.

    Returns:
        float entre 0 et 1: duree du jour en time_of_day
    """
    # cas ou le lever est inferieur au coucher en time_of_day
    if tod_coucher < tod_lever:
        tod_coucher += 1
        
    # d'apres les calculs de minimisation des données de daylight par rapport aux données reelles, il faut enlever 0.0555 environ à la duree du jour calculée avec daylight pour avoir une valeur plus correcte
    if avec_minimisation:
        return tod_coucher - tod_lever  - 0.0555
    else:
        return tod_coucher - tod_lever


if __name__ == "__main__":

    # le fichier json
    filename = 'gps/0f3e7eb6.json'
    data = photoset(filename)

    duree_partition = 3 # en jours
    partitions = photo_set_partiel(data, duree_partition)

    print("nombre de partitions :", len(partitions))
    len_min_part = 2000
    len_max_part = 0
    for p in partitions:
        if len(p) < len_min_part:
            len_min_part = len(p)
        elif len(p) > len_max_part:
            len_max_part = len(p)
    print("nb min de photos dans partitions :", len_min_part)
    print("nb max de photos dans partitions :", len_max_part)
    
    partitions_2point0 = photo_set_partiel_2point0(data, duree_partition, 10, 4)
    print("nombre de partitions 2.0 :", len(partitions_2point0))
    len_min_part = 2000
    len_max_part = 0
    for p in partitions_2point0:
        if len(p) < len_min_part:
            len_min_part = len(p)
        elif len(p) > len_max_part:
            len_max_part = len(p)
    print("nb min de photos dans partitions 2.0:", len_min_part)
    print("nb max de photos dans partitions :", len_max_part)
    
