from class_photoset import photoset
from calcul_coordo import midi,latitude,longitude, score , ponderation_lat
from calcul_lever_coucher import lever_coucher,calculer_duree_du_jour,photo_set_partiel,photo_set_partiel_2point0
import numpy as np
from os.path import join
from analyse_donnees import Correlation_point, affichage_partition, duree_photoset, nb_partitions_daylight_basse
from gestion_datetime import datetime_python_to_time_of_day, get_datetime_python, get_solar_noon_greenwich, datetime_to_datime_py, datetime_to_jours, dans_periode_instable, jour_numero, jour_numero
from geopy.distance import geodesic

def test_nb_jours_partition(fichs):
    best_scores_nb_jours = []
    for f in fichs:
        print(f)
        data = photoset(f)
        best_score = 100000
        best_score_nb = -1
        a_ete_calcule = False
        for nb_jours_partition in range(1, 21):
            #print("Duree de prise de vue du photoset :", datetime_to_jours(duree_photoset(data)), "\nNombre de photos dans le set :", data.get_taille())
            duree_partition = nb_jours_partition
            
            #affichage_partition(data, tod_lever, tod_coucher, midi(tod_lever, tod_coucher))
            
            ### Calcul de la latitude
            # Partitione le jeu de donnee suivant une certaine periode (en jours)
            #duree_partition = 3 # en jours
            duree_max = 7
            nb_min_photo_set = 30
            partitions = photo_set_partiel_2point0(data, duree_partition, nb_min_photo_set, duree_max)

            # pour chaque partition, on realise le calcul de la latitude
            latitudes = []
            # caclucl du midi solaire et de la longitude pour chaque partition
            longitudes = []
            #print(len(partitions))
            for partition in partitions:
                #print(len(partition))
                # commence par le calcul de la duree du jour
                premier_jour = get_datetime_python(partition[0]["image_shooting"])
                # si la partition n'a pas de lever ET de coucher (nb de periodes low daylight < 2), alors on annule le calcul (généralement pour la derniere partition qui n'est pas sur un jour entier)
                if nb_partitions_daylight_basse(partition, partition=True) == 2 and not dans_periode_instable(premier_jour) :
                    tod_lever, tod_coucher = lever_coucher(partition)
                    #print(tod_lever, tod_coucher)

                    ### Latitude
                    # duree du jour
                    duree_jour = calculer_duree_du_jour(tod_lever, tod_coucher)
                    # jour de la premiere photo de la partition
                    lati = latitude(duree_jour, premier_jour)
                    latitudes.append(lati)
                    
                    ### Longitude
                    # calcul midi solaire (necessaire pour calcul longitude apres)
                    ms = midi(tod_lever, tod_coucher)
                    longi = longitude(ms, datetime_python_to_time_of_day(get_solar_noon_greenwich(premier_jour)))
                    longitudes.append(longi)
                    # possibilité ici d'afficher la partition avec lever du jour, coucher et midi_solaire
                    #affichage_partition(partition, tod_lever, tod_coucher, midi(tod_lever, tod_coucher))
                    #print("Premier jour du set :", premier_jour, "Lever :", tod_lever, "Coucher :", tod_coucher, "Midi-solaire :",ms, "Latitude :", lati, "Longitude :", longi)
                    #input()
                
            # ici peut-etre realiser la moyenne des latitudes de la liste latitudes
            if(len(latitudes)==0 or len(longitudes)==0):
                #print("toutes les partitions sont dans la période instable")
                print("", end="")
            else:
                a_ete_calcule = True
                lat = np.mean(latitudes)
                lon = np.mean(longitudes)
                lat2=data.data["photo_set"]["gps"]["latitude"]
                long2=data.data["photo_set"]["gps"]["longitude"]
                ecart_dist = geodesic((lat,lon), (lat2,long2)).km
                if ecart_dist < best_score:
                    best_score = ecart_dist
                    best_score_nb = duree_partition
        if a_ete_calcule:
            best_scores_nb_jours.append(best_score_nb)
    print(best_scores_nb_jours)
    print(np.mean(best_scores_nb_jours))
    
# Programme principal
if __name__ == "__main__":


    # test_nb_jours_partition(['c8094bb1.json', 'e5cb5e82.json', '84b5bb60.json', '69a0b8d1.json', '1ab73e07.json', '85d79a8c.json', '0f3e7eb6.json', '1c7e31db.json', 'a9da00e7.json'])
    # Résultats, nb de jours optimaux de partition suivant duree total photoset:
    # fichiers moins 7 jours : 1 jour
    # fichiers 7 - 3 jours : 3 jours
    # fichiers 30 - 90 jours : 7 jours
    # fichiers 90 - 180 jours : 10 jours
    # fichiers plus 180 jours : 14 jours
    
    # le fichier json
    filename = '0b34ff9e.json'
    data = photoset(filename)
    tod_lever = 0.4
    tod_coucher = 0.6
    print("Duree de prise de vue du photoset :", datetime_to_jours(duree_photoset(data)), "\nNombre de photos dans le set :", data.get_taille())
    duree_partition = int(input("Entrez la duree d'une partition (un entier en jours):\n"))
    
    #affichage_partition(data, tod_lever, tod_coucher, midi(tod_lever, tod_coucher))
    
    ### Calcul de la latitude
    # Partitione le jeu de donnee suivant une certaine periode (en jours)
    duree_max = 7
    nb_min_photo_set = 30
    partitions = photo_set_partiel_2point0(data, duree_partition, nb_min_photo_set, duree_max)
    
    # pour chaque partition, on realise le calcul de la latitude
    latitudes = []
    # caclucl du midi solaire et de la longitude pour chaque partition
    longitudes = []
    sum_ponderations = 0
    #print(len(partitions))
    for partition in partitions:
        #print(len(partition))
        # commence par le calcul de la duree du jour
        premier_jour = get_datetime_python(partition[0]["image_shooting"])
        # si la partition n'a pas de lever ET de coucher (nb de periodes low daylight < 2), alors on annule le calcul (généralement pour la derniere partition qui n'est pas sur un jour entier)
        if nb_partitions_daylight_basse(partition, True) == 2 and not dans_periode_instable(premier_jour) :
            tod_lever, tod_coucher = lever_coucher(partition)
            #print(tod_lever, tod_coucher)

            ### Latitude
            # duree du jour
            duree_jour = calculer_duree_du_jour(tod_lever, tod_coucher)
            # jour de la premiere photo de la partition
            lati = latitude(duree_jour, premier_jour)
            sum_ponderations += abs(ponderation_lat[jour_numero(premier_jour)-1])
            latitudes.append(lati)

            ### Longitude
            # calcul midi solaire (necessaire pour calcul longitude apres)
            ms = midi(tod_lever, tod_coucher)
            longi = longitude(ms, datetime_python_to_time_of_day(get_solar_noon_greenwich(premier_jour)))
            longitudes.append(longi)
            # possibilité ici d'afficher la partition avec lever du jour, coucher et midi_solaire
            #affichage_partition(partition, True, tod_lever, tod_coucher, midi(tod_lever, tod_coucher), True)
            #print("Premier jour du set :", premier_jour, "Lever :", tod_lever, "Coucher :", tod_coucher, "Midi-solaire :",ms, "Latitude :", lati, "Longitude :", longi)
            #input()
        
    # ici peut-etre realiser la moyenne des latitudes de la liste latitudes
    if(len(latitudes)==0 or len(longitudes)==0):
        print("toutes les partitions sont dans la période instable")
    else:
        lat = np.sum(latitudes) / sum_ponderations
        lon = np.mean(longitudes)
        print("Latitude calc :", round(lat,5), "Longitude calc :", round(lon,5))
        if data.gps:
            lat2 = data.latitude
            lon2 = data.longitude
            print("Coord. reelles : lat :", round(lat2,5), "long :", round(lon2,5))
            print("Score de précision :", score((lat,lon),(lat2,lon2)))
    
        
    
    """
    doss = r"D:\Sauvegarde Sab\Cle_usb\Ecole\L3\Stage_applicatif\github\testgit\projet\Classement_no_gps\analysable"
    for f in os.listdir(doss):
        data = photoset(f)
        print(f, data.id, end=' ')
        #lat2 = data.latitude
        #lon2 = data.longitude
        if datetime_to_jours(duree_photoset(data)) < 8:
            if f == "6d6b2948.json":
                duree_partition = 3
            else:
                duree_partition = 1
        elif datetime_to_jours(duree_photoset(data)) < 31:
            duree_partition = 3
        elif datetime_to_jours(duree_photoset(data)) < 91:
            duree_partition = 7
        elif datetime_to_jours(duree_photoset(data)) < 181:
            duree_partition = 10
        else:
            duree_partition = 14
            
        
        #affichage_partition(data, tod_lever, tod_coucher, midi(tod_lever, tod_coucher))
        
        ### Calcul de la latitude
        # Partitione le jeu de donnee suivant une certaine periode (en jours)
        #duree_partition = 3 # en jours
        duree_max = 25
        nb_min_photo_set = 30
        partitions = photo_set_partiel_2point0(data, duree_partition, nb_min_photo_set, duree_max)
                
        # pour chaque partition, on realise le calcul de la latitude
        latitudes_min_pond = []
        # caclucl du midi solaire et de la longitude pour chaque partition
        longitudes = []
        sum_ponderations = 0
        #print(len(partitions))
        for partition in partitions:
            #print(len(partition))
            # commence par le calcul de la duree du jour
            premier_jour = get_datetime_python(partition[0]["image_shooting"])
            # si la partition n'a pas de lever ET de coucher (nb de periodes low daylight < 2), alors on annule le calcul (généralement pour la derniere partition qui n'est pas sur un jour entier)
            if nb_partitions_daylight_basse(partition, partition=True) == 2 and not dans_periode_instable(premier_jour) :
                tod_lever, tod_coucher = lever_coucher(partition)
                #print(tod_lever, tod_coucher)

                ### Latitude
                # duree du jour
                duree_jour_min = calculer_duree_du_jour(tod_lever, tod_coucher)
                # jour de la premiere photo de la partition
                lati_min_pond = latitude(duree_jour_min, premier_jour)
                latitudes_min_pond.append(lati_min_pond)
                try:
                    sum_ponderations += abs(ponderation_lat[jour_numero(premier_jour)-1])
                except IndexError:
                    sum_ponderations += abs(ponderation_lat[jour_numero(premier_jour)-2])
                
                ### Longitude
                # calcul midi solaire (necessaire pour calcul longitude apres)
                ms = midi(tod_lever, tod_coucher)
                longi = longitude(ms, datetime_python_to_time_of_day(get_solar_noon_greenwich(premier_jour)))
                longitudes.append(longi)
                # possibilité ici d'afficher la partition avec lever du jour, coucher et midi_solaire
                #affichage_partition(partition, tod_lever, tod_coucher, midi(tod_lever, tod_coucher))
                #print("Premier jour du set :", premier_jour, "Lever :", tod_lever, "Coucher :", tod_coucher, "Midi-solaire :",ms, "Latitude :", lati, "Longitude :", longi)
                #input()
            
        # ici peut-etre realiser la moyenne des latitudes de la liste latitudes
        if(len(latitudes_min_pond)==0 or len(longitudes)==0):
            print("toutes les partitions sont dans la période instable")
        else:
            # lat = np.mean(latitudes)
            lat_min_pond = np.sum(latitudes_min_pond) / sum_ponderations
            lon = np.mean(longitudes)
            print(get_datetime_python(partitions[0][0]["image_shooting"]), round(datetime_to_jours(duree_photoset(data)), 1), round(lat_min_pond, 6), round(lon, 6))#, round(geodesic((lat,lon), (lat2,lon2)).km, 3), round(geodesic((lat,lon2), (lat2,lon2)).km, 3), round(geodesic((lat2,lon), (lat2,lon2)).km, 3), round(lat_min, 6), round(lon, 6), round(geodesic((lat_min,lon), (lat2,lon2)).km, 3), round(geodesic((lat_min,lon2), (lat2,lon2)).km, 3), round(geodesic((lat2,lon), (lat2,lon2)).km, 3), round(lat_min_pond, 6), round(lon, 6), round(geodesic((lat_min_pond,lon), (lat2,lon2)).km, 3), round(geodesic((lat_min_pond,lon2), (lat2,lon2)).km, 3), round(geodesic((lat2,lon), (lat2,lon2)).km, 3))
            # print("Coord. reelles : lat :", round(data.latitude,5), "long :", round(data.longitude,5))
            # print("Latitude calc :", round(lat,5), "Longitude calc :", round(lon,5))
            # #print(lat,lon)
            # lat2=data.data["photo_set"]["gps"]["latitude"]
            # long2=data.data["photo_set"]["gps"]["longitude"]
            # print("Score de précision :", score((lat,lon),(lat2,long2)))
""" 