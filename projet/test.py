import datetime
from math import radians
from astral.sun import sun
from astral import LocationInfo
import calcul_coordo as cal_co
from gestion_datetime import afficher_time_of_day, datetime_python_to_time_of_day, datetime_to_datime_py, get_datetime_python, get_solar_noon_greenwich, jour_numero
from astral.geocoder import database, lookup
import numpy as np
import matplotlib.pyplot as plt


def ponderation_lat(jour=None):
    ville = "Moscow"
    city = lookup(ville, database())
    lat = f"{city.latitude:.02f}"
    print(lat)
    float_lat = float(lat)
    v = np.linspace(0, 1, 365)
    l = []
    for u in v:
        d = datetime_to_datime_py(2020+u)
        l.append(1/(cal_co.latitude_bis(fonc_test(d, ville), d)-float_lat))
    return l if jour == None else l[jour]

def duree_jour_test(h_coucher,h_lever):
    h_coucher=datetime_python_to_time_of_day(get_datetime_python(h_coucher))
    h_lever=datetime_python_to_time_of_day(get_datetime_python(h_lever))
    if h_coucher < h_lever:
        h_coucher += 1
    return abs(h_coucher-h_lever)


#test longitude & latitude
def test1():
    city=lookup("Ottawa", database())
    date=datetime.datetime.now()
    s=sun(city.observer,date)
    ms_local=get_datetime_python(f'{s["noon"]}')
    ms_green=get_solar_noon_greenwich(date)
    # print("midi solaire local",ms_local)
    # print("midi solaire greenwich",ms_green)
    tod_ms_local=datetime_python_to_time_of_day(ms_local)
    tod_ms_green=datetime_python_to_time_of_day(ms_green)
    print("test1 long calculé : ",cal_co.longitude(tod_ms_local,tod_ms_green))
    print("test1 long réel : ",f"{city.longitude:.04f}")
    h_lever=(f'{s["sunrise"]}')
    h_coucher=(f'{s["sunset"]}')
    #afficher_time_of_day(datetime_python_to_time_of_day(get_datetime_python(h_lever)))
    #print("____",get_datetime_python(h_city=lookup("Ottawa", database())
    duree_jour=duree_jour_test(h_coucher,h_lever)
    #afficher_time_of_day(duree_jour)
    print("test1 lat calculé : ",cal_co.latitude(duree_jour,date))
    print("test1 lat réel : ",f"{city.latitude:.04f}")

def test2():
    city=lookup("Ottawa", database())
    date=datetime.datetime(2021, 6, 24)
    s=sun(city.observer,date)
    ms_local=get_datetime_python(f'{s["noon"]}')
    ms_green=get_solar_noon_greenwich(date)
    # print("midi solaire local",ms_local)
    # print("midi solaire greenwich",ms_green)
    tod_ms_local=datetime_python_to_time_of_day(ms_local)
    tod_ms_green=datetime_python_to_time_of_day(ms_green)
    print("test2 long calculé : ",cal_co.longitude(tod_ms_local,tod_ms_green))
    print("test2 long réel : ",f"{city.longitude:.04f}")
    h_lever=(f'{s["sunrise"]}')
    h_coucher=(f'{s["sunset"]}')
    #afficher_time_of_day(datetime_python_to_time_of_day(get_datetime_python(h_lever)))
    #print("____",get_datetime_python(h_lever))
    duree_jour=duree_jour_test(h_coucher,h_lever)
    #afficher_time_of_day(duree_jour)
    print("test2 lat calculé : ",cal_co.latitude(duree_jour,date))
    print("test2 lat réel : ",f"{city.latitude:.04f}")

# test1()
# test2()


# date1 =datetime.datetime(2019,9,21,12,0,0)
# date2 =datetime.datetime(2019,9,21,12,0,0)
# ttt=datetime_python_to_time_of_day(date1)
# ccc=datetime_python_to_time_of_day(date2)
# print("test___ delta calculé : ",abs(cal_co.latitude(ttt,date1) - cal_co.latitude(ccc,date2)))


def fonc_test(datetime_py,ville):
    city=lookup(ville,database())
    s=sun(city.observer,datetime_py)
    h_lever=(f'{s["sunrise"]}')
    h_coucher=(f'{s["sunset"]}')
    return duree_jour_test(h_coucher,h_lever)



def fonc2():
    ville = "Libreville"
    city=lookup(ville,database())
    lat=f"{city.latitude:.02f}"
    print(lat)
    float_lat=float(lat)
    v=np.linspace(0,1,365)
    l=[]
    for u in v:
        d=datetime_to_datime_py(2020+u)
        l.append(cal_co.latitude(fonc_test(d,ville),d,False,False)-float_lat)
    plt.plot(v,l)
    plt.plot([1,0],[0,0])
    plt.title("Ecart latitude réelle et latitude calculée")
    plt.xlabel("année normalisée",color='r')
    plt.ylabel("latitude",color='b')
    plt.show()



def fonc3():
    ville="Ottawa"
    city=lookup(ville,database())
    long=f"{city.longitude:.02f}"
    print(long)
    float_long=float(long)
    v=np.linspace(0,1,365)
    l=[]
    for u in v:
        d=datetime_to_datime_py(2020+u)
        s=sun(city.observer,d)
        ms_local=get_datetime_python(f'{s["noon"]}')
        ms_green=get_solar_noon_greenwich(d)
        tod_ms_local=datetime_python_to_time_of_day(ms_local)
        tod_ms_green=datetime_python_to_time_of_day(ms_green)
        l.append(cal_co.longitude(tod_ms_local,tod_ms_green)-float_long)
    plt.plot(v,l)
    plt.plot([1,0],[0,0])
    plt.ylim(-180,180)
    plt.title("Ecart longitude réelle et longitude calculée")
    plt.xlabel("année normalisée",color='r')
    plt.ylabel("longitude",color='b')
    plt.show()

def erreur_lat(ville):
    city=lookup(ville,database())
    lat=f"{city.latitude:.02f}"
    float_lat=float(lat)
    v=np.linspace(0,1,365)
    l=[]
    for u in v:
        d=datetime_to_datime_py(2020+u)
        l.append(cal_co.latitude(fonc_test(d,ville),d)-float_lat)
    return v,l


def datetime_critique(tab_jour,tab_latitude,seuil):
    i=0
    l=[]
    taille=len(tab_jour)
    while(i<taille and tab_latitude[i]>-seuil):
        i=i+1
    l.append(tab_jour[i])
    while(i<taille and tab_latitude[i]<seuil):
        i=i+1
    while(i<taille and tab_latitude[i]>seuil):
        i=i+1
    l.append(tab_jour[i])
    while(i<taille and tab_latitude[i]<seuil):
        i=i+1
    l.append(tab_jour[i])
    while(i<taille and tab_latitude[i]>-seuil):
        i=i+1
    while(i<taille and tab_latitude[i]<-seuil):
        i=i+1
    l.append(tab_jour[i])
    return l



tab_ville = ["Aberdeen", "Abu Dhabi", "Abuja", "Accra", "Addis Ababa", "Adelaide", "Al Jubail", "Albany", "Albuquerque", "Algiers", "Amman", "Amsterdam", "Andorra la Vella", "Ankara", "Annapolis", "Antananarivo", "Apia", "Ashgabat", "Asmara", "Astana", "Asuncion", "Athens", "Atlanta", "Augusta", "Austin", "Avarua", "Baghdad", "Baku", "Baltimore", "Bamako", "Bandar Seri Begawan", "Bangkok", "Bangui", "Banjul", "Barrow-In-Furness", "Basse-Terre", "Basseterre", "Baton Rouge", "Beijing", "Beirut", "Belfast", "Belgrade", "Belmopan", "Berlin", "Bern", "Billings", "Birmingham", "Birmingham", "Bishkek", "Bismarck", "Bissau", "Bloemfontein", "Bogota", "Boise", "Bolton", "Boston", "Bradford", "Brasilia", "Bratislava", "Brazzaville", "Bridgeport", "Bridgetown", "Brisbane", "Bristol", "Brussels", "Bucharest", "Bucuresti", "Budapest", "Buenos Aires", "Buffalo", "Bujumbura", "Burlington", "Cairo", "Canberra", "Cape Town", "Caracas", "Cardiff", "Carson City", "Castries", "Cayenne", "Charleston", "Charlotte", "Charlotte Amalie", "Cheyenne", "Chicago", "Chisinau", "Cleveland", "Columbia", "Columbus", "Conakry", "Concord", "Copenhagen", "Cotonou", "Crawley", "Dakar", "Dallas", "Damascus", "Dammam", "Denver", "Des Moines", "Detroit", "Dhaka", "Dili", "Djibouti", "Dodoma", "Doha", "Douglas", "Dover", "Dublin", "Dushanbe", "Edinburgh", "El Aaiun", "Fargo", "Fort-de-France", "Frankfort", "Freetown", "Funafuti", "Gaborone", "George Town", "Georgetown", "Gibraltar", "Glasgow", "Greenwich", "Guatemala", "Hanoi", "Harare", "Harrisburg", "Hartford", "Havana", "Helena", "Helsinki", "Hobart", "Hong Kong", "Honiara", "Honolulu", "Houston", "Indianapolis", "Islamabad", "Jackson", "Jacksonville", "Jakarta", "Jefferson City", "Jerusalem", "Juba", "Jubail", "Juneau", "Kabul", "Kampala", "Kansas City", "Kathmandu", "Khartoum", "Kiev", "Kigali", "Kingston", "Kingston", "Kingstown", "Kinshasa", "Koror", "Kuala Lumpur", "Kuwait", "La Paz", "Lansing", "Las Vegas", "Leeds", "Leicester", "Libreville", "Lilongwe", "Lima", "Lincoln", "Lisbon", "Little Rock", "Liverpool", "Ljubljana", "Lome", "London", "Los Angeles", "Louisville", "Luanda", "Lusaka", "Luxembourg", "Macau", "Madinah", "Madison", "Madrid", "Majuro", "Makkah", "Malabo", "Male", "Mamoudzou", "Managua", "Manama", "Manchester", "Manchester", "Manila", "Maputo", "Maseru", "Masqat", "Mbabane", "Mecca", "Medina", "Melbourne", "Memphis", "Mexico", "Miami","Milwaukee", "Minneapolis", "Minsk", "Mogadishu", "Monaco","Monrovia", "Montevideo", "Montgomery","Montpelier", "Moroni", "Moscow", "Moskva", "Mumbai", "Muscat", "Nairobi", "Nashville", "Nassau", "Naypyidaw", "New Delhi", "New Orleans", "New York", "Newark", "Newcastle", "Newcastle Upon Tyne", "Ngerulmud", "Niamey", "Nicosia", "Norwich", "Nouakchott", "Noumea", "Oklahoma City", "Olympia", "Omaha", "Oranjestad", "Orlando", "Oslo", "Ottawa", "Ouagadougou", "Oxford", "Pago Pago", "Palikir", "Panama", "Papeete", "Paramaribo", "Paris", "Perth", "Philadelphia", "Phnom Penh", "Phoenix", "Pierre", "Plymouth", "Podgorica", "Port Louis", "Port Moresby", "Port of Spain", "Port-Vila", "Port-au-Prince", "Portland", "Portland", "Porto-Novo", "Portsmouth", "Prague", "Praia", "Pretoria", "Pristina", "Providence", "Quito", "Rabat", "Raleigh", "Reading", "Richmond", "Riga", "Riyadh", "Road Town", "Rome", "Roseau", "Sacramento", "Saint Helier", "Saint Paul", "Saint Pierre", "Saipan", "Salem", "Salt Lake City", "San Diego", "San Francisco", "San Jose", "San Juan", "San Marino", "San Salvador", "Sana", "Santa Fe", "Santiago", "Santo Domingo", "Sao Tome", "Sarajevo", "Seattle", "Seoul", "Sheffield", "Singapore", "Sioux Falls", "Skopje", "Sofia", "Southampton", "Springfield", "Sri Jayawardenapura Kotte", "Stanley", "Stockholm", "Sucre", "Suva", "Swansea", "Swindon", "Sydney", "Taipei", "Tallahassee", "Tallinn", "Tarawa", "Tashkent", "Tbilisi", "Tegucigalpa", "Tehran","Thimphu", "Tirana","Tirane", "Tokyo", "Toledo", "Topeka", "Trenton", "Tripoli", "Tunis", "Ulaanbaatar", "Ulan Bator", "Vaduz", "Valletta", "Vienna", "Vientiane", "Vilnius", "Virginia Beach", "Warsaw", "Washington DC", "Wellington", "Wichita", "Willemstad", "Wilmington", "Windhoek", "Wolverhampton", "Yamoussoukro", "Yangon", "Yaounde", "Yaren", "Yerevan", "Zagreb"]

def fonc4():
    seuil=2
    g=[0,0,0,0]
    j=0
    for ville in tab_ville:
        print(ville)
        t1,t2=erreur_lat(ville)
        l=datetime_critique(t1,t2,seuil)
        g=[g[i]+l[i] for i in range(0,len(g))]
        j=j+1
        print(j)
    taille=len(tab_ville)
    g=[i/taille for i in g]
    print(g)
    return g
    #l = [l1[i] + l2[i] for i in range(0, len(l1))]
    #l = [i+val for i in l]
    

#fonc4()


#print(ponderation_lat())

fonc2()
fonc3()

#test normalise_datetime_python
# print("test normalise_datetime_python : \n")
# print(normalise_datetime_python(get_datetime_python("2020-05-03T12:11:04")))
# print(normalise_datetime_python(get_datetime_python("2019-10-31T11:19:02"))==2019.8314289066464)
# print(normalise_datetime_python(get_datetime_python("2020-05-03T12:11:04"))==2020.3374526917628)