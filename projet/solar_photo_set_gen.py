import solar 
import json 
import string
import random
json_file = []

def Genere_nom():
    number_of_strings = 5
    length_of_string = 8
    while True:
        for x in range(number_of_strings):
            name = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(length_of_string))
        if name not in json_file:
            break
    name = name + ".json"
    json_file.append(name)
    return name

def Create_json_file(filename,dictionnaire):
    with open(filename) as outfile:
        json.dump(dictionnaire, outfile)




if __name__ == "__main__":
    print(Genere_nom())
    print(json_file)
