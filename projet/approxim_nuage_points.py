import numpy as np
import matplotlib.pyplot as plt
from numpy.polynomial.polynomial import polyfit
from class_photoset import photoset

# modelise le sous-set
def analyse_nuage_de_point(data,analysable = False, non_correlation=False,non_outdoor = False):
    """
    Affiche tous les points de daylight et de brightness d'un photoset en fonction de time_of_day 
    
    Args:
        data(class photoset) : un objet de class photoset
        analysable(bool) : Si data est analysable
        non_correlaton(bool) : Si data est  non correlée
        non_outdoor(bool) : Si la plupaat des photos de data est indoor        
    """
    time_of_day = data.get_time_of_day() 
    brightness = data.get_brightness()
    daylight = data.get_daylight()

    # approximation second degre methode des moindres carrés du nuage de points (time_of_day, brightness)
    pol = polyfit(time_of_day, daylight, 2)
    # polynome
    indice = np.arange (0,101,1)
    x = indice/100
    y = pol[0] + pol[1]*x + pol[2]*x*x

    # affichage des données
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    #fig  = plt.figure(figsize=(10,10))
    # Trace de la brightness en fonction du time_of_day
    ax1.plot(time_of_day, brightness, 'b.')
    ax1.set_xlabel('time_of_day')
    ax1.set_ylabel('brightness', color='b')
    for tl in ax1.get_yticklabels():
        tl.set_color('b')
    

    # Trace de la daylight en fonction du time_of_day
    ax2.plot(time_of_day, daylight, 'r.')
    ax2.set_ylabel('daylight', color='r')
    ax2.set_xlabel('time_of_day', color='b')
    for tl in ax2.get_yticklabels():
        tl.set_color('r')
    ax2.plot(x, y)
    if analysable == True :
        plt.title("Données analysable",color="green")
    elif non_correlation == True :
        plt.title("Données Brightness et Daylight non correlé",color= "orange" )
    elif non_outdoor == True :
        plt.title("Données dont la plupart des photos sont indoor",color="red") 

    plt.show()

if __name__ == "__main__":
    # Data analysable 
    filename = "0b34ff9e"
    data = photoset(filename)
    analyse_nuage_de_point(data,analysable = True)

    # non correlation
    filename = "27e41163"
    data = photoset(filename)
    analyse_nuage_de_point(data,non_correlation=True)

    # non outdoor
    filename = "0c577c14"
    data = photoset(filename)
    analyse_nuage_de_point(data, non_outdoor=True)
